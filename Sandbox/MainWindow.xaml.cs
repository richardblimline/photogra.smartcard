﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sandbox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Photogra.SmartCard.CardReader _reader;

        public MainWindow()
        {
            InitializeComponent();

            this.Loaded += OnLoaded;
            this.Closing += OnClosing;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            _reader = new Photogra.SmartCard.CardReader { Options = Photogra.SmartCard.ReaderOptions.UidOnly };
            _reader.Card += OnCard;
            _reader.Open();
        }

        private void OnCard(object sender, Photogra.SmartCard.Card card)
        {
            if (card is Photogra.SmartCard.CardType.MifareUltralightC)
            {
                var fidelioCard = new Photogra.SmartCard.CardVendor.Fidelio((Photogra.SmartCard.CardType.MifareUltralightC)card);

                System.Diagnostics.Debug.WriteLine(fidelioCard.CardIdentificationNumber);
                System.Diagnostics.Debug.WriteLine(fidelioCard.DebarkationDate.ToString("yyyy-MM-dd"));
            }
            else if (card is Photogra.SmartCard.CardType.Mifare1K)
            {
                System.Diagnostics.Debug.WriteLine(BitConverter.ToString(card.ID));
            }
        }

        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_reader != null)
            {
                _reader.Dispose();
                _reader = null;
            }
        }
    }
}
