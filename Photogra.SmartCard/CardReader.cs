﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Photogra.SmartCard
{
    public delegate void CardEventHandler(object sender, Card card);

    public class CardReader : IDisposable
    {
        private IntPtr _hContext = IntPtr.Zero;
        private IntPtr _hCard = IntPtr.Zero;

        private bool _isOpen;
        private bool _hasCard;

        private readonly System.Threading.SynchronizationContext _synchronizationContext;
        private System.Threading.Thread _currentThread;
        private string[] _cardReaders;
        private string _cardReader;

        private const byte MAX_APDU_SIZE = 0xff;

        public event CardEventHandler Card;

        public CardReader()
        {
            _synchronizationContext = System.ComponentModel.AsyncOperationManager.SynchronizationContext;
            _currentThread = System.Threading.Thread.CurrentThread;

            this.Pattern = "^(HID|OMNIKEY)";
            this.Options = ReaderOptions.None;
        }

        public bool Open()
        {
            this.Close();

            // Establish context.
            var ret = WinSCard.SCardEstablishContext(WinSCard.SCARD_SCOPE_USER, IntPtr.Zero, IntPtr.Zero, out _hContext);

            if (ret != WinSCard.SCARD_S_SUCCESS)
                return false;

            uint pcchReaders = 0;

            ret = WinSCard.SCardListReaders(_hContext, null, null, ref pcchReaders);

            if (ret != WinSCard.SCARD_S_SUCCESS)
                return false;

            byte[] mszReaders = new byte[pcchReaders];

            // Fill readers buffer with second call.
            ret = WinSCard.SCardListReaders(_hContext, null, mszReaders, ref pcchReaders);

            if (ret != WinSCard.SCARD_S_SUCCESS)
                return false;

            // Populate List with readers.
            string currbuff = Encoding.ASCII.GetString(mszReaders);
            int len = (int)pcchReaders;
            int nullindex = -1;
            char nullchar = (char)0;

            var readers = new List<string>();

            if (len > 0)
            {
                while (currbuff[0] != nullchar)
                {
                    nullindex = currbuff.IndexOf(nullchar);   // Get null end character.
                    string reader = currbuff.Substring(0, nullindex);

                    readers.Add(reader);

                    len = len - (reader.Length + 1);
                    currbuff = currbuff.Substring(nullindex + 1, len);
                }
            }

            _cardReaders = readers.ToArray();

            if (readers.Count > 0)
            {
                _cardReader = GetCardReaderRegEx();
                _isOpen = true;

                this.RunPollingThread();
            }
            else
            {
                this.Close();

                return false;
            }

            return true;
        }

        public void Close()
        {
            _isOpen = false;

            if (_hCard != IntPtr.Zero)
            {
                var ret = WinSCard.SCardDisconnect(_hCard, WinSCard.SCARD_UNPOWER_CARD);

                _hCard = IntPtr.Zero;
            }

            if (_hContext != IntPtr.Zero)
            {
                var ret = WinSCard.SCardReleaseContext(_hContext);

                _hContext = IntPtr.Zero;
            }
        }

        private void RunPollingThread()
        {
            // Let's not use the thread pool for this long running thread.
            var thread = new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                WinSCard.SCARD_READERSTATE readerState;

                uint valueTimeout; //The maximum amount of time to wait for an action
                uint readerCount; //Count for number of readers

                while (_isOpen && _currentThread != null && _currentThread.IsAlive)
                {
                    readerState.RdrName = _cardReader;
                    readerState.RdrCurrState = WinSCard.SCARD_STATE_UNAWARE;
                    readerState.RdrEventState = 0;
                    readerState.UserData = "Mifare Card";
                    readerState.ATRLength = 0;
                    readerState.ATRValue = null;

                    valueTimeout = 0;
                    readerCount = 1;

                    var ret = WinSCard.SCardGetStatusChange(_hContext, valueTimeout, ref readerState, readerCount);

                    //if (ret != WinSCard.SCARD_S_SUCCESS)
                    //    return;

                    if (ret != WinSCard.SCARD_S_SUCCESS || readerState.ATRLength == 0)
                    {
                        if (_hasCard)
                        {
                            // SmartCard Removed
                            _hasCard = false;
                        }
                    }
                    else
                    {
                        if (!_hasCard)
                        {
                            // SmartCard Inserted
                            _hasCard = true;

                            //// This card reader is slow to initialize?
                            //if (System.Text.RegularExpressions.Regex.IsMatch(_cardReader, "^HID OMNIKEY 5427", System.Text.RegularExpressions.RegexOptions.None))
                            //    System.Threading.Thread.Sleep(500);

                            var card = this.ReadCard(readerState);

                            if (card != null)
                            {
                                _synchronizationContext.Send(new System.Threading.SendOrPostCallback(state =>
                                {
                                    this.Card?.Invoke(this, (Card)state);
                                }), card);
                            }
                        }
                    }

                    System.Threading.Thread.Sleep(500);
                }
            }));

            thread.Start();
        }

        private Card ReadCard(WinSCard.SCARD_READERSTATE readerState)
        {
            var cardType = readerState.ATRValue[readerState.ATRLength - 6];

            Card card = null;
            IntPtr protocol = IntPtr.Zero;

            var ret = WinSCard.SCardConnect(_hContext, _cardReader, WinSCard.SCARD_SHARE_SHARED, WinSCard.SCARD_PROTOCOL_T1, ref _hCard, ref protocol);

            if (ret != WinSCard.SCARD_S_SUCCESS)
                return null;

            if (cardType == WinSCard.Card_Type_Mifare_1K)
            {
                card = new CardType.Mifare1K();

                this.ReadID(card);

                if ((Options & ReaderOptions.UidOnly) != ReaderOptions.UidOnly)
                {
                    if (this.LoadKey(0x20, 0x00, new byte[] { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff }) && this.GeneralAuthenticate(WinSCard.Mifare_Key_A, 0x00))
                    {
                        this.ReadPage(card, 0x00, Page);
                        //this.ReadPage(card, 0x00, 0x01);
                    }
                }
            }
            else if (cardType == WinSCard.Card_Type_Mifare_Ultralight_C)
            {
                card = new CardType.MifareUltralightC();

                this.ReadID(card);
                //this.ReadPage(card, 0x00, 0x10);
                this.ReadPage(card, 0x00, Page);
            }

            if (card != null)
            {
                card.ATR = new byte[readerState.ATRLength];

                Array.Copy(readerState.ATRValue, card.ATR, readerState.ATRLength);
                Array.Copy(readerState.ATRValue, readerState.ATRLength - 13, card.RID, 0, 5);
            }

            ret = WinSCard.SCardDisconnect(_hCard, WinSCard.SCARD_UNPOWER_CARD);

            _hCard = IntPtr.Zero;

            return card;
        }

        private bool ReadID(Card card)
        {
            if (_hCard == IntPtr.Zero)
                return false;

            byte[] sendBuffer = new byte[] { 0xff, 0xca, 0x00, 0x00, 0x00 };
            byte[] receiveBuffer = new byte[MAX_APDU_SIZE];

            WinSCard.SCARD_IO_REQUEST sendRequest;
            sendRequest.dwProtocol = WinSCard.SCARD_PROTOCOL_T1;
            sendRequest.cbPciLength = 8;
            WinSCard.SCARD_IO_REQUEST receiveRequest;
            receiveRequest.dwProtocol = WinSCard.SCARD_PROTOCOL_T1;
            receiveRequest.cbPciLength = 8;

            uint receiveBufferLen = MAX_APDU_SIZE;

            var ret = WinSCard.SCardTransmit(_hCard, ref sendRequest, sendBuffer, (uint)sendBuffer.Length, ref receiveRequest, receiveBuffer, ref receiveBufferLen);

            if (ret == WinSCard.SCARD_S_SUCCESS && receiveBufferLen > 1 && receiveBuffer[receiveBufferLen - 2] == 0x90)
            {
                card.ID = new byte[receiveBufferLen - 2];

                Array.Copy(receiveBuffer, card.ID, receiveBufferLen - 2);

                return true;
            }

            return false;
        }

        private bool ReadPage(Card card, byte sector, byte page)
        {
            if (_hCard == IntPtr.Zero)
                return false;

            byte[] sendBuffer = new byte[] { 0xff, 0xb0, sector, page, 0x10 };
            byte[] receiveBuffer = new byte[MAX_APDU_SIZE];

            WinSCard.SCARD_IO_REQUEST sendRequest;
            sendRequest.dwProtocol = WinSCard.SCARD_PROTOCOL_T1;
            sendRequest.cbPciLength = 8;
            WinSCard.SCARD_IO_REQUEST receiveRequest;
            receiveRequest.dwProtocol = WinSCard.SCARD_PROTOCOL_T1;
            receiveRequest.cbPciLength = 8;

            uint receiveBufferLen = MAX_APDU_SIZE;

            var ret = WinSCard.SCardTransmit(_hCard, ref sendRequest, sendBuffer, (uint)sendBuffer.Length, ref receiveRequest, receiveBuffer, ref receiveBufferLen);

            if (ret == WinSCard.SCARD_S_SUCCESS && receiveBufferLen > 1 && receiveBuffer[receiveBufferLen - 2] == 0x90)
            {
                int j = 0;

                for (int i = page; i < page + (receiveBufferLen - 2) / card.PageSize; i++)
                {
                    card.Pages[i] = new byte[card.PageSize];

                    Array.Copy(receiveBuffer, j, card.Pages[i], 0, card.PageSize);

                    j += card.PageSize;
                }

                return true;
            }

            return false;
        }

        private bool LoadKey(byte keyStructure, byte keyNumber, byte[] key)
        {
            if (_hCard == IntPtr.Zero || key == null)
                return false;

            byte[] sendBuffer = new byte[] { 0xff, 0x82, keyStructure, keyNumber, (byte)key.Length };
            byte[] receiveBuffer = new byte[MAX_APDU_SIZE];

            var sendBufferLen = sendBuffer.Length;

            Array.Resize(ref sendBuffer, sendBufferLen + key.Length);
            Array.Copy(key, 0, sendBuffer, sendBufferLen, key.Length);

            WinSCard.SCARD_IO_REQUEST sendRequest;
            sendRequest.dwProtocol = WinSCard.SCARD_PROTOCOL_T1;
            sendRequest.cbPciLength = 8;
            WinSCard.SCARD_IO_REQUEST receiveRequest;
            receiveRequest.dwProtocol = WinSCard.SCARD_PROTOCOL_T1;
            receiveRequest.cbPciLength = 8;

            uint receiveBufferLen = MAX_APDU_SIZE;

            var ret = WinSCard.SCardTransmit(_hCard, ref sendRequest, sendBuffer, (uint)sendBuffer.Length, ref receiveRequest, receiveBuffer, ref receiveBufferLen);

            if (ret == WinSCard.SCARD_S_SUCCESS && receiveBufferLen > 1 && receiveBuffer[receiveBufferLen - 2] == 0x90)
                return true;

            return false;
        }

        private bool GeneralAuthenticate(byte keyType, byte keyNumber)
        {
            if (_hCard == IntPtr.Zero)
                return false;

            byte[] sendBuffer = new byte[] { 0xff, 0x86, 0x00, 0x00, 0x05, 0x01, 0x00, 0x01, keyType, keyNumber };
            byte[] receiveBuffer = new byte[MAX_APDU_SIZE];

            WinSCard.SCARD_IO_REQUEST sendRequest;
            sendRequest.dwProtocol = WinSCard.SCARD_PROTOCOL_T1;
            sendRequest.cbPciLength = 8;
            WinSCard.SCARD_IO_REQUEST receiveRequest;
            receiveRequest.dwProtocol = WinSCard.SCARD_PROTOCOL_T1;
            receiveRequest.cbPciLength = 8;

            uint receiveBufferLen = MAX_APDU_SIZE;

            var ret = WinSCard.SCardTransmit(_hCard, ref sendRequest, sendBuffer, (uint)sendBuffer.Length, ref receiveRequest, receiveBuffer, ref receiveBufferLen);

            if (ret == WinSCard.SCARD_S_SUCCESS && receiveBufferLen > 1 && receiveBuffer[receiveBufferLen - 2] == 0x90)
                return true;

            return false;
        }

        public bool IsOpen
        {
            get
            {
                return _isOpen;
            }
        }

        private string GetCardReaderRegEx()
        {
            if (this.Pattern != null)
                foreach (var cardReader in _cardReaders)
                    if (System.Text.RegularExpressions.Regex.IsMatch(cardReader, this.Pattern, System.Text.RegularExpressions.RegexOptions.None))
                        return cardReader;

            return _cardReaders.First();
        }

        public string Pattern { get; set; }
        public byte Page { get; set; }
        public ReaderOptions Options { get; set; }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                this.Close();

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~CardReader()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
    }

    [Flags]
    public enum ReaderOptions { None, UidOnly }
}
