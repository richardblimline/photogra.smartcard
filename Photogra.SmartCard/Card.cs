﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Photogra.SmartCard
{
    public abstract class Card
    {
        public byte[] ID { get; internal set; }
        internal byte[] RID { get; private set; }
        internal byte[] ATR { get; set; }
        public byte[][] Pages { get; internal set; }
        internal int PageSize { get; set; }

        public Card()
        {
            this.RID = new byte[5];
        }
    }
}
