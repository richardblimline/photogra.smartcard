﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Photogra.SmartCard.CardType
{
    public class Mifare1K : Card
    {
        public Mifare1K() : base()
        {
            this.Pages = new byte[0x3f][];
            this.PageSize = 16;
        }
    }
}
