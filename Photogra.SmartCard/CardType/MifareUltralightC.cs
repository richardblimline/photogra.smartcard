﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Photogra.SmartCard.CardType
{
    public class MifareUltralightC : Card
    {
        public MifareUltralightC() : base()
        {
            //this.ID = new byte[7];
            this.Pages = new byte[0x2f][];
            this.PageSize = 4;
        }
    }
}
