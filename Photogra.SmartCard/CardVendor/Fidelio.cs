﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Photogra.SmartCard.CardVendor
{
    public class Fidelio
    {
        private readonly CardType.MifareUltralightC _card;

        public Fidelio(CardType.MifareUltralightC card)
        {
            _card = card;
        }

        public string CardIdentificationNumber
        {
            get
            {
                if (_card != null && _card.Pages[0x10] != null && _card.Pages[0x11] != null)
                {
                    var bytes = new byte[8];

                    Array.Copy(_card.Pages[0x10], 0, bytes, 0, 4);
                    Array.Copy(_card.Pages[0x11], 0, bytes, 4, 4);

                    var cardIdentificationNumber = BitConverter.ToUInt64(bytes, 0);

                    if (cardIdentificationNumber != 0)
                        return cardIdentificationNumber.ToString("D16");
                }

                return null;
            }
        }

        public DateTime DebarkationDate
        {
            get
            {
                if (_card != null && _card.Pages[0x12] != null)
                {
                    DateTime result;
                    uint integerDateTime = BitConverter.ToUInt32(_card.Pages[0x12], 0);

                    if (DateTime.TryParseExact(integerDateTime.ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None, out result))
                        return result;
                }

                return new DateTime();
            }
        }
    }
}
